{
    "id": "813046d4-54dd-4270-98d0-47b8b6b6885b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0e3ea8a-9865-4cd0-8e5a-2eb16e52f8eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "813046d4-54dd-4270-98d0-47b8b6b6885b",
            "compositeImage": {
                "id": "16bdd117-3e93-4e41-8814-654d68add526",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0e3ea8a-9865-4cd0-8e5a-2eb16e52f8eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9524e79c-d7fa-4b25-94b5-5d6d5a2aae75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0e3ea8a-9865-4cd0-8e5a-2eb16e52f8eb",
                    "LayerId": "e3976e4e-25ff-4406-b8c7-dc773c75ec05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e3976e4e-25ff-4406-b8c7-dc773c75ec05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "813046d4-54dd-4270-98d0-47b8b6b6885b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}