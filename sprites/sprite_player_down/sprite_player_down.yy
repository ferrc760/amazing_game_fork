{
    "id": "afaa78a9-4cf3-4708-8381-c43b5b88ec6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9159c89c-82e2-40c3-83e0-304a5de1dc35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afaa78a9-4cf3-4708-8381-c43b5b88ec6b",
            "compositeImage": {
                "id": "d28cb47a-e7d1-4256-9ca5-d59e30b40663",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9159c89c-82e2-40c3-83e0-304a5de1dc35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe677702-adc9-4c4c-9b7b-028906cf9e0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9159c89c-82e2-40c3-83e0-304a5de1dc35",
                    "LayerId": "322290fe-82b4-4641-89b6-040ed025d8e2"
                }
            ]
        },
        {
            "id": "f7ef2fcb-e69f-4488-b7a7-ec8738c34e9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afaa78a9-4cf3-4708-8381-c43b5b88ec6b",
            "compositeImage": {
                "id": "6b3c4016-5f86-4958-b97c-24e6a6c375f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7ef2fcb-e69f-4488-b7a7-ec8738c34e9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "feea0463-0781-47f0-90e3-acda4468842f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7ef2fcb-e69f-4488-b7a7-ec8738c34e9d",
                    "LayerId": "322290fe-82b4-4641-89b6-040ed025d8e2"
                }
            ]
        },
        {
            "id": "982b183a-03d1-422d-9dd6-736736551e96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afaa78a9-4cf3-4708-8381-c43b5b88ec6b",
            "compositeImage": {
                "id": "cff98425-1b19-4809-871e-472a53ec6034",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "982b183a-03d1-422d-9dd6-736736551e96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "384ffc28-6a78-44bf-8403-336f6e669d42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "982b183a-03d1-422d-9dd6-736736551e96",
                    "LayerId": "322290fe-82b4-4641-89b6-040ed025d8e2"
                }
            ]
        },
        {
            "id": "ced45fba-125a-4ce5-9aab-92da1cc3eed0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afaa78a9-4cf3-4708-8381-c43b5b88ec6b",
            "compositeImage": {
                "id": "a2da7c49-8608-44ba-87bc-78c3d14e5f99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ced45fba-125a-4ce5-9aab-92da1cc3eed0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b09f2cd-8a2a-4e3e-818a-6dd4e810372b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ced45fba-125a-4ce5-9aab-92da1cc3eed0",
                    "LayerId": "322290fe-82b4-4641-89b6-040ed025d8e2"
                }
            ]
        },
        {
            "id": "9beeb15e-86ed-4c95-98d4-aeb980610f54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afaa78a9-4cf3-4708-8381-c43b5b88ec6b",
            "compositeImage": {
                "id": "06d0037d-8e83-4f99-8891-7c6d5154a5de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9beeb15e-86ed-4c95-98d4-aeb980610f54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "383d61b0-69fb-4df7-af26-2d125b049d22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9beeb15e-86ed-4c95-98d4-aeb980610f54",
                    "LayerId": "322290fe-82b4-4641-89b6-040ed025d8e2"
                }
            ]
        },
        {
            "id": "9a8fc979-3c62-4d25-9997-2c6b93713d9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afaa78a9-4cf3-4708-8381-c43b5b88ec6b",
            "compositeImage": {
                "id": "426c9eea-2f1a-45df-a691-2e49a9e386fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a8fc979-3c62-4d25-9997-2c6b93713d9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66392f15-c213-4b87-a8a7-13b64cc87bdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a8fc979-3c62-4d25-9997-2c6b93713d9d",
                    "LayerId": "322290fe-82b4-4641-89b6-040ed025d8e2"
                }
            ]
        },
        {
            "id": "368ae46c-b9c8-4c63-a879-16e6897e251b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afaa78a9-4cf3-4708-8381-c43b5b88ec6b",
            "compositeImage": {
                "id": "b6c2dd9a-9f11-489c-9aa5-733b869591bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "368ae46c-b9c8-4c63-a879-16e6897e251b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f22e56ec-9946-41e3-942d-880cb62da365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "368ae46c-b9c8-4c63-a879-16e6897e251b",
                    "LayerId": "322290fe-82b4-4641-89b6-040ed025d8e2"
                }
            ]
        },
        {
            "id": "dfc21177-03c6-4cf0-9aca-19168d244497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afaa78a9-4cf3-4708-8381-c43b5b88ec6b",
            "compositeImage": {
                "id": "821fba0c-3935-4bd7-b427-734d566b3e70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfc21177-03c6-4cf0-9aca-19168d244497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "156c9b61-2845-4e1c-a867-f7528be61126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfc21177-03c6-4cf0-9aca-19168d244497",
                    "LayerId": "322290fe-82b4-4641-89b6-040ed025d8e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "322290fe-82b4-4641-89b6-040ed025d8e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afaa78a9-4cf3-4708-8381-c43b5b88ec6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}